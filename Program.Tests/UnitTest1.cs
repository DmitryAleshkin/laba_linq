using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Program;

namespace Program.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1() //Тест определяет студента с наибольшим средним баллом
        {   
            List<Student> students = new List<Student>()
            {
                new Student {name = "Igor", group = "First", exam = "English", mark = 3},    //4.33
                new Student {name = "Igor", group = "First", exam = "Math", mark = 5},
                new Student {name = "Igor", group = "First", exam = "Biology", mark = 5},
                new Student {name = "Alexey", group = "Third", exam = "English", mark = 2},   //4.2
                new Student {name = "Alexey", group = "Third", exam = "Math", mark = 5},
                new Student {name = "Alexey", group = "Third", exam = "Math.Model", mark = 5},
                new Student {name = "Alexey", group = "Third", exam = "Geometry", mark = 5},
                new Student {name = "Alexey", group = "Third", exam = "Biology", mark = 4},
                new Student {name = "Anna", group = "Second", exam = "English", mark = 5},    //4.75
                new Student {name = "Anna", group = "Second", exam = "Math", mark = 4},
                new Student {name = "Anna", group = "Second", exam = "Geometry", mark = 5},
                new Student {name = "Anna", group = "Second", exam = "Biology", mark = 5},
            };
            string[] res = {"Anna"};

            Assert.Equal(res, Class1.Max_AverageMark(students));
        }

        [Fact]
        public void Test2() //Вычислить средний балл по каждому предмету
        {
            List<Student> students = new List<Student>()
            {
                new Student {name = "Igor", group = "First", exam = "English", mark = 3},             //English = 3.33
                new Student {name = "Igor", group = "First", exam = "Math", mark = 5},                //Math = 4.66
                new Student {name = "Igor", group = "First", exam = "Biology", mark = 5},             //Biology = 4.66
                new Student {name = "Alexey", group = "Third", exam = "English", mark = 2},           //Geometry = 5
                new Student {name = "Alexey", group = "Third", exam = "Math", mark = 5},              //Math.Model = 5 (одна переменная)
                new Student {name = "Alexey", group = "Third", exam = "Math.Model", mark = 5},
                new Student {name = "Alexey", group = "Third", exam = "Geometry", mark = 5},
                new Student {name = "Alexey", group = "Third", exam = "Biology", mark = 4},
                new Student {name = "Anna", group = "Second", exam = "English", mark = 5},
                new Student {name = "Anna", group = "Second", exam = "Math", mark = 4},
                new Student {name = "Anna", group = "Second", exam = "Geometry", mark = 5},
                new Student {name = "Anna", group = "Second", exam = "Biology", mark = 5},
            };

            string[] res = {"Biology 4,67",
                            "Geometry 5",
                            "English 3,33",
                            "Math 4,67",
                            "Math.Model 5"};
            Array.Sort(res);
            Assert.Equal(res, Class1.AverageMarkOfexam(students));
        }
    
        [Fact]
        public void Test3() //По каждому предмету определить группу с лучшим средним баллом
        {
            List<Student> students = new List<Student>()
            {
                new Student {name = "Igor", group = "First", exam = "English", mark = 2},             // English Second 4
                new Student {name = "Igor", group = "First", exam = "Math", mark = 5},                // Biology Third 4,5
                new Student {name = "Igor", group = "First", exam = "Biology", mark = 5},             // Math First 5
                new Student {name = "Maria", group = "First", exam = "Biology", mark = 3},
                new Student {name = "Maria", group = "First", exam = "Math", mark = 5},
                new Student {name = "Maria", group = "First", exam = "English", mark = 5},            
                
                new Student {name = "Alexey", group = "Third", exam = "English", mark = 2},          
                new Student {name = "Alexey", group = "Third", exam = "Math", mark = 5},             
                new Student {name = "Alexey", group = "Third", exam = "Biology", mark = 4},
                new Student {name = "Igor", group = "Third", exam = "English", mark = 3},
                new Student {name = "Igor", group = "Third", exam = "Math", mark = 3},
                new Student {name = "Igor", group = "Third", exam = "Biology", mark = 5},
                
                new Student {name = "Anna", group = "Second", exam = "English", mark = 5},
                new Student {name = "Anna", group = "Second", exam = "Math", mark = 4},
                new Student {name = "Anna", group = "Second", exam = "Biology", mark = 4},
                new Student {name = "Egor", group = "Second", exam = "English", mark = 3},
                new Student {name = "Egor", group = "Second", exam = "Math", mark = 2},
                new Student {name = "Egor", group = "Second", exam = "Biology", mark = 3}
            };

            string[] res = {"English Second 4", "Biology Third 4,5", "Math First 5",};
            Array.Sort(res);
            Assert.Equal(res, Class1.TheBestGroupInExam(students));

        }
    
    }
}
