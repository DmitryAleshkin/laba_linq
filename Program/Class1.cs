﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Dynamic;
using System.Security.Principal;
namespace Program
{
   //public record Student (string name, string group, string exam, double mark);

    public class Student
    {
        public string? name;
        public string? group;
        public string? exam;
        public double mark;
    }

    public class Class1
    {
        public static IEnumerable<string?> Max_AverageMark(List<Student> students)
        {                        
            var GroupNameAndAvgMark = students
                .GroupBy(n => n.name,
                         m => m.mark, 
                         (name, mark) => new {name, average = Math.Round(mark.Average() * 100) / 100});
            var maxMark = GroupNameAndAvgMark
                .Max(m => m.average);
            return GroupNameAndAvgMark
                .Where(s => s.average == maxMark)
                .Select(s => s.name)
                .OrderBy(s => s);                  
        }
        public static IEnumerable<string?> AverageMarkOfexam(List<Student> students)
        {
            var GroupByExamAvgMark = students
                .GroupBy(e => e.exam,
                         m => m.mark,
                         (exam, mark) => new {exam, average = Math.Round(mark.Average() * 100) / 100})
                .OrderBy(s => s.exam);

            return GroupByExamAvgMark
                .Select(s => $"{s.exam} {s.average}");
        }
        
        public static IEnumerable<string?> TheBestGroupInExam(List<Student> students)
        {
            var ExamGroup = students
                .GroupBy(s => new {s.exam, s.group, s.mark})
                .Select(s => new Student()
                        {
                            exam = s.Key.exam,
                            group = s.Key.group,
                            mark = Math.Round(students
                                          .Where(st => st.exam == s.Key.exam && st.group == s.Key.group)
                                          .Select(st => st.mark)
                                          .Average() * 100) / 100
                            
                        });
                
            return ExamGroup
                .Where(eg => eg.mark == ExamGroup
                        .Where(egs => egs.exam == eg.exam)
                        .Select(am => am.mark)
                        .Max())
                .Select(eg => $"{eg.exam} {eg.group} {eg.mark.ToString()}")
                .Distinct()
                .OrderBy(ob => ob);
              
        }
    }
}